//write a c++ program to implement virtual base class. assume suitable data members and each of these classes should have a getdata() function to get its data from the user at the keyboard and putdata() function to display data.
#include<iostream>
using namespace std;
class Employee
{
protected:char name[20];
		  double salary;
public: void getdata()
		{
			cout<<"Input the employee name and salary\n";
			cin>>name>>salary;
		}
		void putdata()
		{
			cout<<"The employee information is\n";
			cout<<name<<endl<<salary<<endl;
		}
};
class manager:virtual public Employee
{
protected:int ssn;
public: void getdata()
		{
			cout<<"Input the manager name,ssn and salary\n";
			cin>>name>>ssn>>salary;
		}
		void putdata()
		{
			cout<<"The manager information is\n";
			cout<<name<<endl<<ssn<<endl<<salary<<endl;
		}
};
class laborer:virtual public Employee
{
protected:char designation[20];
public: void getdata()
		{
			cout<<"Input the laborer name,designation and salary\n";
			cin>>name>>designation>>salary;
		}
		void putdata()
		{
			cout<<"The laborer information is\n";
			cout<<name<<endl<<designation<<endl<<salary<<endl;
		}
};
class scientist:virtual public Employee
{
protected:char department[20];
public: void getdata()
		{
			cout<<"Input the scientist name,department and salary\n";
			cin>>name>>department>>salary;
		}
		void putdata()
		{
			cout<<"The scientist information is\n";
			cout<<name<<endl<<department<<endl<<salary<<endl;
		}
};
class foreman:public manager,public laborer
{
protected:char place[20];
public: void getdata()
		{
			cout<<"Input the foreman name,place and salary\n";
			cin>>name>>place>>salary;
		}
		void putdata()
		{
			cout<<"The foreman information is\n";
			cout<<name<<endl<<place<<endl<<salary<<endl;
		}
};
int main()
{
    Employee e;
	e.getdata();
	e.putdata();
	manager m;
    m.getdata();
	m.putdata();
	laborer l;
	l.getdata();
	l.putdata();
	scientist s;
    s.getdata();
	s.putdata();
	foreman f;
	f.getdata();
	f.putdata();
}