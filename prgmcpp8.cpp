/*Design a template named array_ class add functions that perform the following tasks:
Display last element in an array
Display an element in a specified position in an array(the argument specifies the position)
Reverse the order of elements in an array
Sort elements in an array in ascending order
Sort elements in an array in descending order
Sum elements in an array
*/
#include<iostream>
using namespace std;
template <class array_class>
void array_fun(array_class x[10],int n)//generic function defination.
{
	array_class y[10];
	char ch;
	int top=0;
	for(;;)
	{
		cout<<"\na) Display last element in an array\nb) Display an element specified position in an array\nc) Reverce the order of elements in an array\nd) Sort elements in ascending order\ne) Sort elements in descending order\nf) Sum elements in an array\ng) Exit\n";
		cout<<"Input your choice\n";
		cin>>ch;
		switch(ch)
		{
		case 'a':cout<<"The last element in an array is:";
			  while(top<n-1)
			  {
				 top++;
			  }
			  cout<<x[top]<<endl;
			break;
		case 'b': int po;
			      cout<<"Input position to display an element in an array\n";
			      cin>>po;
				  if(po>n || po<0)
				  {
                    cout<<"This is Invalid position\n";
				  } else {
					  cout<<"GIVEN POSITION AN ARRAY ELEMENT IS:"<<x[po-1]<<endl; 
				    }
                  break;
		case 'c':  cout<<"The reverce order element in an array is:\n";
			            for(int i=n-1; i>=0; i--)
					    cout<<x[i]<<"\t";
						cout<<endl;
			     break;
		case 'd' :for(int i=0; i<n; i++)
				  {
					  for(int j=0; j<n-1; j++)
					  {
						  if(x[j]>x[j+1])
						  {
							  int temp=x[j];
							  x[j]=x[j+1];
							  x[j+1]=temp;
						  }
					  }
				  }
				  cout<<"The sorted element in ascending order is:\n";
				  for(int i=0; i<n; i++)
				  {
					  cout<<x[i]<<"\t";
				  }
				  break;
		case 'e':for(int i=0; i<n; i++)
				  {
					  for(int j=0; j<n-1; j++)
					  {
						  if(x[j]<x[j+1])
						  {
							  int temp=x[j];
							  x[j]=x[j+1];
							  x[j+1]=temp;
						  }
					  }
				  }
				  cout<<"The sorted element in deascending order is:\n";
				  for(int i=0; i<n; i++)
				  {
					  cout<<x[i]<<"\t";
				  }
				  break;
		case 'f':int sum;
			     for(int i=0; i<n; i++)
				 {
					 
					 sum+=x[i];
				 }
				 cout<<"Sum element in an array is:"<<sum<<endl;
                 break;
		case 'g':exit(0);
	default:cout<<"This is Invalid choice\n";
		}
	}
}

int main()
{
	int n,a[10];
	cout<<"INPUT THE SIZE OF AN ARRAY\n";
	cin>>n;
	cout<<"INPUT THE ELEMENT IN AN ARRAY\n";
	for(int i = 0; i < n; i++)
	{
		cin>>a[i];
	}
	array_fun(a,n); //call here generic function
	return 0;
}