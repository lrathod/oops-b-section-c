#include<iostream>
	#include<string>
	using namespace std;
	 
	class StudentGrade
	{
	    private:
	        int idnum;
	        string sName;
	        double tScore;
	        double gradePoint;
	        string gradeLetter;
	    public:
	        StudentGrade(int,double,double,string,string);
	        void setIdnum(const int);
	        void setName(int);
	        void setScore(double);
	        void setGradePoint(double);
	        void setGradeLetter();
	        void displayGrade();
	        int getIdnum();
	        double getScore();
	        double getGrade();
	};
	StudentGrade::StudentGrade(int id, double score, double point, string name, string letter)
	{
	    idnum = id;
	    sName = name;
	    tScore = score;
	    gradePoint = point;
	    gradeLetter = letter;
	}
	void StudentGrade::setIdnum(const int Id)
	{
	    idnum = Id;
	}
	void StudentGrade::setName(int name)
	{
	    sName = name;
	}
    void StudentGrade::setScore(double score)
	{
	    tScore = score;
	}
	void StudentGrade::setGradePoint(double point)
	{
	    if(point < 0 || point >=100)
	    {
	        cout << "Invalid entry..." << endl;
	    }
	    else
	        gradePoint = point;
	}
	void StudentGrade::setGradeLetter(string letter, string let)
	{
	    if (let >= 90)
	        letter = 'A';
	    else
	        if (let > 80 && let <= 89)
	            letter = 'B';
	        else
	            if (let > 70 && let <=79)
	                letter = 'C';
	            else
	                if (let > 60 && let <= 69)
	                    letter = 'D';
	                else
	                    if (let < 60)
	                        letter = 'F';
	 
	    gradeLetter = letter;
	}
	void StudentGrade::displayGrade()
	{
	    cout << "ID # " << idnum << " Name: " << sName << " Grade: " << gradeLetter << endl;
	}
	int main(string name, int id, string grade)
	{
	    cout << "Enter Grade: ";
	    cin >> grade;
	 
	    cout << "Enter ID #: ";
	    cin >> id;
	 
	    cout << "Enter name: ";
	    cin >> name;
	 
    StudentGrade student(grade, id, name);
	    student.displayGrade();
	 
	    return 0;
}