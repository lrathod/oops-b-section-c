/*Create base class called shape. Use this class to store two double type values that could be used to compute the area of figures. Derive two specific classes called triangle and rectangle and circle from the base class. Add to the base class, a member function get_data() to initialize base class data members and display_area() to compute and display the area of figures. Make display_area()  as a virtual function.*/
#include<iostream>
using namespace std;
class shape
{
protected:
	double x;
	double y;
public: 
	void get_data()
	{
		cin>>x>>y;
	}
	virtual void display_area(){};
};
class triangle:public shape
{
	double tr_area;
public:
	void display_area()
	{
		tr_area=0.5*x*y;
		cout<<"Area of triangle is:"<<tr_area<<endl;
	}
};
class rectangle:public shape
{
	double rt_area;
public:
	void display_area()
	{
		rt_area=x*y;
		cout<<"Area of rectangle is="<<rt_area<<endl;
	}
};
class circle:public shape
{
	double cr_area;
public:
	void display_area()
	{
		cr_area=3.142*x*x;
		cout<<"circle of area is="<<cr_area<<endl;
	}
};
int main()
{
	int option;
	shape *pt;
	shape art;
	triangle t1;
	rectangle rt;
	circle c1;
	do
	{
		cout<<"To compute area of \n\t1.Triangle"<<"\n\t2.Rectangle\n\t3.Circle\n\t4.EXIT\n"<<endl;
		cout<<"Input your choice"<<endl;
		cin>>option;
		switch(option)
		{
		case 1:cout<<"\n compute area of triangle \n"<<endl;
			   cout<<"Input the values of x and y as base and height:\n";
               pt=&t1;
			   pt->get_data();
			   pt->display_area();
			   break;
		
	    case 2:cout<<"\n compute area of rectangle\n";
			   cout<<"Input the values of x and y as length and bredth:\n";
               pt=&rt;
			   pt->get_data();
			   pt->display_area();
			   break;

	   case 3: cout<<"\n compute area of circle \n";
			   cout<<"Input the values of r:\n";
			   pt=&c1;
               pt->get_data();
			   pt->display_area();
			   break;
	   case 4: cout<<"\nTHANK YOU VISIT AGAIN"<<endl;
				       exit(0);
					   break;

	   default: cout<<"\THIS IS INVALID CHOICE"<<endl;
		}
	}while(option!=4);
}
			   