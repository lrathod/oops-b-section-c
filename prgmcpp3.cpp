/*Write C++ program to Implement Matrix Class to perform addition and subtraction of two matrix using Constructor, Destructor, Copy Constructor, Overload an assignment operator ,+ ,- operator and allocate the memory dynamically. The following output should be generated. */
#include<iostream>
using namespace std;
class matrix
{
int a[20][20];
public:
	int m,n;
	matrix()
	{
		m = 0;
		n = 0;
	}
	matrix(int a,int b)
	{
		m = a;
		n = b;
	}
	~matrix()
	{
		m;
		n;
	}
	void getdata();
	void display();
	matrix operator+(matrix &m2);
	matrix operator-(matrix &m2);
};
void matrix::getdata()
{
	for(int i = 0; i < m; i++)
		for(int j = 0; j < n; j++)
			cin>>a[i][j];
}
void matrix::display()
{
	for(int i = 0; i < m; i++)
	{
		for(int j = 0; j < n; j++)
		{
			cout<<a[i][j]<<"\t";
		}
		cout<<"\n";
	}
}

matrix matrix::operator +(matrix &m2)
{
	matrix temp;
	temp.m = m2.m;
	temp.n = m2.n;
	for(int i = 0; i < m; i++)
		for(int j = 0; j < n; j++)
			temp.a[i][j] = a[i][j]+m2.a[i][j];
	 return (temp);
}
matrix matrix::operator -(matrix &m2)
{
	matrix temp;
	temp.m = m;
	temp.n = n;
	for(int i = 0; i < m; i++)
		for(int j = 0; j < n; j++)
			temp.a[i][j] =a[i][j]-m2.a[i][j];
	 return (temp);
}

void main()
{
    matrix m1(2,2),m2(2,2),m3,m4;
	cout<<"Input the element for matrix 1\n";
	m1.getdata();
	cout<<"Input the element for matrix 2\n";
	m2.getdata();
	cout<<"Matrix A is....\n"; 
	m1.display();
	cout<<"Matrix B is....\n"; 
	m2.display();
	m3 = m1+m2;
	cout<<"c=a+b....\n";
	m3.display();
	m3=m1-m2;
	cout<<"c=a-b....\n";
	m3.display();
	m4 = m3;
	cout<<"....copy constructor is invoked....\n";
    m4.display();
}


		


	





